# EvoBSD

EvoBSD is an ansible project used for customising OpenBSD hosts used by Evolix.

## How to install an OpenBSD machine

**Note :** The system must be installed with a root account only.

1.  Install ansible's prerequisites

```
ansible-playbook prerequisite.yml -CDi hosts -l HOSTNAME
```

2.  Run it

The variables files evolix-main.yml and evolinux-secrets.yml are customized variables for Evolix that overwrite main.yml variables. They are not needed if you are not from Evolix.

First use (become_method: su) :

```
ansible-playbook evolixisation.yml --ask-vault-pass -CDki hosts -u root -l HOSTNAME
```

Subsequent use (become_method: sudo) :

```
ansible-playbook evolixisation.yml --ask-vault-pass -CDKi hosts --skip-tags pf -l HOSTNAME
```

## How to update scripts

Several tags in the format `*utils` are configured to update the different scripts, to be used with the update-utils.yml playbook :

* evomaintenance-utils : update scripts from evomaintenance role, main.yml task
* etc-git-utils : update scripts from etc-git role, utils.yml task
* utils : update scripts from base role, utils.yml task
* nagios-nrpe-utils : update scripts and checks from nagios-nrpe role, main.yml task
* evocheck-utils : update scripts from evocheck role, main.yml task
* evobackup-utils : update scripts from evobackup role, main.yml task
* motd-utils : update script from post-install role, motd.yml task

## Contributions

See the [contribution guidelines](CONTRIBUTING.md)

## License

[MIT License](LICENSE)

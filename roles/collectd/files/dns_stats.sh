#!/bin/sh

UNBOUND_RAWSTATS=$(doas /usr/sbin/unbound-control stats)

echo "PUTVAL $(hostname)/dns_unbound-query_count/count-queries N:$(echo "$UNBOUND_RAWSTATS" | grep total.num.queries= | cut -d= -f2 )"

echo "PUTVAL $(hostname)/dns_unbound-cache/count-hits N:$(echo "$UNBOUND_RAWSTATS" | grep total.num.cachehits= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-cache/count-miss N:$(echo "$UNBOUND_RAWSTATS" | grep total.num.cachemiss= | cut -d= -f2)"

echo "PUTVAL $(hostname)/dns_unbound-query_type/count-A N:$(echo "$UNBOUND_RAWSTATS" | (grep num.query.type.A= || echo 0) | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-query_type/count-AAAA N:$(echo "$UNBOUND_RAWSTATS" | (grep num.query.type.AAAA= || echo 0) | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-query_type/count-NS N:$(echo "$UNBOUND_RAWSTATS" | (grep num.query.type.NS= || echo 0) | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-query_type/count-PTR N:$(echo "$UNBOUND_RAWSTATS" | (grep num.query.type.PTR= || echo 0) | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-query_type/count-MX N:$(echo "$UNBOUND_RAWSTATS" | (grep num.query.type.MX= || echo 0) | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-query_type/count-TXT N:$(echo "$UNBOUND_RAWSTATS" | (grep num.query.type.TXT= || echo 0) | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-query_type/count-SRV N:$(echo "$UNBOUND_RAWSTATS" | (grep num.query.type.SRV= || echo 0) | cut -d= -f2)"

echo "PUTVAL $(hostname)/dns_unbound-answer_rcode/count-NOERROR N:$(echo "$UNBOUND_RAWSTATS" | grep num.answer.rcode.NOERROR= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-answer_rcode/count-SERVFAIL N:$(echo "$UNBOUND_RAWSTATS" | grep num.answer.rcode.SERVFAIL= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-answer_rcode/count-NXDOMAIN N:$(echo "$UNBOUND_RAWSTATS" | grep num.answer.rcode.NXDOMAIN= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-answer_rcode/count-REFUSED N:$(echo "$UNBOUND_RAWSTATS" | grep num.answer.rcode.REFUSED= | cut -d= -f2)"

echo "PUTVAL $(hostname)/dns_unbound-histogram/count-0_to_1µs N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000000.000000.to.000000.000001= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-1_to_2µs N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000000.000001.to.000000.000002= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-2_to_4µs N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000000.000002.to.000000.000004= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-4_to_8µs N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000000.000004.to.000000.000008= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-8_to_16µs N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000000.000008.to.000000.000016= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-16_to_32µs N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000000.000016.to.000000.000032= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-32_to_64µs N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000000.000032.to.000000.000064= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-64_to_128µs N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000000.000064.to.000000.000128= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-128_to_256µs N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000000.000128.to.000000.000256= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-256_to_512µs N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000000.000256.to.000000.000512= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-0.5_to_1ms N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000000.000512.to.000000.001024= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-1_to_2ms N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000000.001024.to.000000.002048= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-2_to_4ms N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000000.002048.to.000000.004096= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-4_to_8ms N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000000.004096.to.000000.008192= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-8_to_16ms N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000000.008192.to.000000.016384= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-16_to_32ms N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000000.016384.to.000000.032768= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-32_to_65ms N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000000.032768.to.000000.065536= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-65_to_131ms N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000000.065536.to.000000.131072= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-131_to_262ms N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000000.131072.to.000000.262144= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-262_to_524ms N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000000.262144.to.000000.524288= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-0.5_to_1s N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000000.524288.to.000001.000000= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-1_to_2s N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000001.000000.to.000002.000000= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-2_to_4s N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000002.000000.to.000004.000000= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-4_to_8s N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000004.000000.to.000008.000000= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-8_to_16s N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000008.000000.to.000016.000000= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-16_to_32s N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000016.000000.to.000032.000000= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-32_to_64s N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000032.000000.to.000064.000000= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-64_to_128s N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000064.000000.to.000128.000000= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-128_to_256s N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000128.000000.to.000256.000000= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-256_to_512s N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000256.000000.to.000512.000000= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-512_to_1024s N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.000512.000000.to.001024.000000= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-1024_to_2048s N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.001024.000000.to.002048.000000= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-2048_to_4096s N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.002048.000000.to.004096.000000= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-4096_to_8192s N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.004096.000000.to.008192.000000= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-8192_to_16384s N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.008192.000000.to.016384.000000= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-16384_to_32768s N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.016384.000000.to.032768.000000= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-32768_to_65536s N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.032768.000000.to.065536.000000= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-65536_to_131072s N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.065536.000000.to.131072.000000= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-131072_to_262144s N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.131072.000000.to.262144.000000= | cut -d= -f2)"
echo "PUTVAL $(hostname)/dns_unbound-histogram/count-262144_to_524288s N:$(echo "$UNBOUND_RAWSTATS" | grep histogram.262144.000000.to.524288.000000= | cut -d= -f2)"

# Munin

Installation and custom configuration of a Munin node

## Tasks

Everything is in the `tasks/main.yml` file.

## Available variables

The full list of variables (with default values) can be found in `defaults/main.yml`.


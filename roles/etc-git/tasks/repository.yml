# yamllint disable rule:line-length
---

- name: "{{ repository_path }} is versioned with git"
  ansible.builtin.command: "git init ."
  args:
    chdir: "{{ repository_path }}"
    creates: "{{ repository_path }}/.git/"
  register: git_init
  tags:
    - etc-git

- name: "Git user.email is configured"
  community.general.git_config:
    name: user.email
    repo: "{{ repository_path }}"
    scope: local
    value: "root@{{ inventory_hostname }}.{{ general_technical_realm }}"
  ignore_errors: '{{ ansible_check_mode }}'
  tags:
    - etc-git

- name: "{{ repository_path }}/.git is restricted to root"
  ansible.builtin.file:
    path: "{{ repository_path }}/.git"
    owner: root
    mode: "0700"
    state: directory
  tags:
    - etc-git

- name: "Some entries MUST be in the {{ repository_path }}/.gitignore file"
  ansible.builtin.lineinfile:
    dest: "{{ repository_path }}/.gitignore"
    line: "{{ item }}"
    owner: root
    mode: "0600"
    create: true
  loop: "{{ gitignore_items | default([]) }}"
  tags:
    - etc-git

- name: "Set vim as default editor"
  community.general.git_config:
    name: core.editor
    scope: global
    value: vim
  ignore_errors: '{{ ansible_check_mode }}'
  tags:
    - etc-git

- name: "Does {{ repository_path }}/ have any commit?"
  ansible.builtin.command: "git log"
  args:
    chdir: "{{ repository_path }}"
  changed_when: false
  failed_when: false
  register: git_log
  check_mode: false
  tags:
    - etc-git

- name: "Initial commit is present?"
  ansible.builtin.shell: "git add -A . && git commit -m \"Initial commit via Ansible\""
  args:
    chdir: "{{ repository_path }}"
  register: git_commit
  when: git_log.rc != 0 or (git_init is defined and git_init is changed)
  tags:
    - etc-git

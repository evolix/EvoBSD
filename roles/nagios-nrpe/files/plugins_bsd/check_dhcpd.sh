#!/bin/sh

# Simple wraper to define the number of dhcpd processes that must be running
# depending on the CARP active or backup state
# Can be customized if dhcpd must be running even in backup state

dhcpd_processes_number=1

CARP_STATUS=$(/sbin/ifconfig carp0 | /usr/bin/grep "status" | /usr/bin/awk '{print $2}')
if [ "$CARP_STATUS" = "backup" ]; then
    dhcpd_processes_number=0
fi

/usr/local/libexec/nagios/check_procs -c ${dhcpd_processes_number}:${dhcpd_processes_number} -C dhcpd
